import {createStore, compose} from '@reduxjs/toolkit';
import Reducer from './reducers'

const initState = {
    string: "oui",

    number:{
        first:0,
        last:10
    },
}

const store = createStore(Reducer,initState,compose( window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()));
export default store 
