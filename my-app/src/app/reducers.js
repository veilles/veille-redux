import {machin,truc, clickOnName} from './actions';

//const initState = "oui"

const initState = {
    string: "oui",

    number:{
        first:0,
        last:10
    }
}

// function Reducer(state,action){
//     if(state){
//         return state
//     }
//     return initState
// }

function Reducer(state=initState,action){
    switch(action.type){
        case machin:
            
            return {
                ...state,
                string: machin
            }
        
        case truc:
            
            return {
                ...state,
                string: truc
            }

        case "reset":
            return {
                ...state,
                string:initState.string,
                number:{
                    first:initState.number.first,
                    last:initState.number.last
                }
            }
            
        case "increment":
            return Object.assign({},state,{
                number:{
                    first: state.number.first+1,
                    last : state.number.last
                }
            })
       
    }
    return state
}
export default Reducer